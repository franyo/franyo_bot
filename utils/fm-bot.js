var request = require('request');

var TAG = "FM-BOT:";
var cLog;

var setup = function (env, logger) {
    cLog = logger;
};

var pageAccessToken = 'EAAYW4RbCMzcBADuGn7YZBjyJp0rZCyVUQtfoitVV5hyON9aCLYXma4xFuIVZAcAX9nAqM1zn4KLv48wZBdpfoltv0idX8GuNGMb3FMq3Oosulyy8qom41ALZCyaMUaRBzyzZBEGsKeTo4zvRMOaVn8vKbRu8pThnYYf5wjVltGRQZDZD';

var getUserData = function (fbuid, onReceive) {
    request({
        url: 'https://graph.facebook.com/v2.6/' + fbuid,
        qs: {access_token: pageAccessToken},
        method: 'GET'
    }, function (error, response, body) {
        if (!!error) {
            cLog(TAG, 'Error sending message: ' + error);
        } else if (!!body.error) {
            cLog(TAG, 'Error: ' + response.body.error);
        } else {
            onReceive(JSON.parse(body));
        }
    });
};

var sendTextMessage = function (fbuid, text, onSent) {
    var requestContent = {
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {access_token: pageAccessToken},
        method: 'POST',
        json: {
            recipient: {id: fbuid},
            message: {text: text}
        }
    };
    request(requestContent, function (error, response, body) {
        if (!!error) {
            cLog(TAG, 'Error sending message: ' + error);
        } else if (!!body.error) {
            cLog(TAG, 'sendTextMessage:', 'Error:', JSON.stringify(response.body.error));
            cLog(TAG, fbuid);
        } else if (onSent) {
            onSent();
        }
    });
};

var sendAction = function (fbuid) {
    var messageObject = {
        recipient: {
            id: fbuid
        },
        message: {
            attachment: {
                type: 'template',
                payload: {
                    template_type: 'generic',
                    elements: [
                        {
                            title: 'What do you want to do next?',
                            image_url: 'https://scontent.ftsr1-1.fna.fbcdn.net/v/t1.0-9/13139191_1705564263040200_4107398222992301532_n.png?oh=74b259081db23c8ae34c661ba20bf84e&oe=57D36FB8',
                            buttons: [
                                {
                                    type: 'web_url',
                                    url: 'http://fleximap.hu/',
                                    title: 'Flexisys website'
                                },
                                {
                                    type: 'postback',
                                    title: 'Start Chatting',
                                    payload: 'START_CHATTING'
                                },
                                {
                                    type: 'postback',
                                    title: 'Leave me alone',
                                    payload: 'LEAVE_ME_ALONE'
                                }
                            ]
                        }
                    ]
                }
            }
        }
    };
    var requestContent = {
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {access_token: pageAccessToken},
        method: 'POST',
        json: messageObject
    };
    request(requestContent, function (error, response, body) {
        if (!!error) {
            cLog(TAG, 'Error sending message: ' + error);
        } else if (!!body.error) {
            cLog(TAG, 'sendTextMessage:', 'Error:', JSON.stringify(response.body.error));
            cLog(TAG, fbuid);
        }
    });
};

module.exports.setup = setup;
module.exports.getUserData = getUserData;
module.exports.sendTextMessage = sendTextMessage;
module.exports.sendAction = sendAction;