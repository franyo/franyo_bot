var ctrls = angular.module('portalApp.controllers', []);

ctrls.controller('PortalCtrl', function ($scope, $http) {
    $scope.data = {};
    $scope.data.users = [];
    $scope.data.message = '';

    $http({method: 'GET', url: '/api/users'}).success(function (data) {
        $scope.data.drivers = [];
        for (var key in data) {
            $scope.data.users.push(data[key]);
        }
    }).error(function () {
        $scope.data.users = [];
    });

    $scope.data.selectedUser = undefined;
    $scope.selectUser = function (driver) {
        $scope.data.selectedUser = driver;
    };

    $scope.isUserSelected = function (driver) {
        if ($scope.data.selectedUser && driver.id == $scope.data.selectedUser.id) {
            return true;
        }
        return false;
    };

    $scope.sendMessage = function () {
        if ($scope.data.selectedUser) {
            var url = '/api/send/' + $scope.data.selectedUser.fbuid + "/" + $scope.data.message;
            $http({method: 'GET', url: url}).success(function () {
            }).error(function () {
            });
        }
    };

    $scope.sendAction = function () {
        if ($scope.data.selectedUser) {
            var url = '/api/action/' + $scope.data.selectedUser.fbuid;
            $http({method: 'GET', url: url}).success(function () {
            }).error(function () {
            });
        }
    };
});