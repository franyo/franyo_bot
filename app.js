const env = process.env;

var express = require('express');
var app = express();
var http = require('http');
var server = http.createServer(app);

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

var dateFormatter = require('./utils/date-formatter');
var bot = require('./utils/fm-bot');
bot.setup(env, cLog);
var mySQLConnection = require('./utils/mysql-connector');
mySQLConnection.setup(env, cLog);

app.get('/health', function (req, res) {
    var logMessage = '/health called';
    cLog(logMessage);

    res.writeHead(200);
    res.end();
});

// STATIC

app.use(express.static('public'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/public/index.html");
});

app.get('/sDSDf345SDFq3e23asd', function (req, res) {
    res.sendFile(__dirname + "/static/portal.html");
});

app.get('/js/:filename', function (req, res) {
    res.sendFile(__dirname + "/static/js/" + req.params.filename);
});

app.get('/css/:filename', function (req, res) {
    res.sendFile(__dirname + "/static/css/" + req.params.filename);
});

// FLEXISYS API

app.get('/api/users', function (req, res) {
    mySQLConnection.listBotUsers(function (users) {
        var userJson = JSON.stringify(users);
        res.send(userJson);
    });
});

app.get('/api/send/*', function (req, res) {
    if (req.params.length < 1) {
        res.send('userId url param required');
    } else {
        var params = req.params[0].split('/');
        var fbuid = params[0];
        var message = '';
        for (var i = 1; i < params.length; i++) {
            if (message.length > 0) {
                message += ' ';
            }
            message += params[i];
        }
        if (message.length == 0) {
            var millis = Date.now();
            message = 'ping: ' + dateFormatter.millisToDateTime(millis);
        }
        cLog('@GET /api/send/', fbuid, '-', message);
        bot.sendTextMessage(fbuid, message);

        res.writeHead(200);
        res.end();
    }
});


app.get('/api/action/:fbuid', function (req, res) {
    var fbuid = req.params.fbuid;
    cLog('@GET /api/action/:', fbuid);
    bot.sendAction(fbuid);

    res.writeHead(200);
    res.end();
});

// FACEBOOK MESSENGER BOT API

app.get('/webhook/', function (req, res) {
    cLog('webhook ellenőrzés megtörtént');
    if (req.query['hub.verify_token'] === 'fbbbbot') {
        res.send(req.query['hub.challenge']);
    } else {
        res.send('Error, wrong validation token');
    }
});

app.post('/webhook/', jsonParser, function (req, res) {
    cLog('@POST /webhook');
    var body = req.body;

    var messaging_events = body.entry[0].messaging;
    for (var i = 0; i < messaging_events.length; i++) {
        var event = body.entry[0].messaging[i];
        var fbuid = event.sender.id;
        if (event.message && event.message.text) {
            var text = event.message.text;
            var send_time = event.timestamp;

            mySQLConnection.saveReceivedMessage(fbuid, text, JSON.stringify(event), send_time, false, function () {
                mySQLConnection.isFirstContact(fbuid,
                    function () {
                        bot.getUserData(fbuid, function (userData) {
                            var full_json = JSON.stringify(userData),
                                first_name = userData.first_name,
                                last_name = userData.last_name,
                                email = userData.email;
                            mySQLConnection.saveBotUser(fbuid, send_time, first_name, last_name, email, full_json);
                            bot.sendTextMessage(fbuid, "Welcome " + first_name + "!", function () {
                                bot.sendTextMessage(fbuid, "I am a wonderful bot to talk to!");
                            });
                        });
                    },
                    function () {
                        bot.sendTextMessage(fbuid, 'What do yo mean by "' + text.substring(0, 200) + '"?');
                    });
            });
        } else {
            var eventString = JSON.stringify(event);
            if (eventString.indexOf('START_CHATTING') > -1) {
                bot.sendTextMessage(fbuid, 'Okey-doke!', function () {
                    bot.sendTextMessage(fbuid, 'How are you?');
                });
            } else if (eventString.indexOf('LEAVE_ME_ALONE') > -1) {
                bot.sendTextMessage(fbuid, 'Oh, ok!', function () {
                    bot.sendTextMessage(fbuid, "I'm sorry it didn't work out...!");
                });
            }
        }
    }
    res.sendStatus(200);
});

// UTIL

function cLog() {
    var millis = Date.now();
    var message = dateFormatter.millisToDateTime(millis);
    for (var i = 0; i < arguments.length; i++) {
        var separator = " ";
        if (i == 0) {
            separator = " : ";
        }
        message += separator;
        message += arguments[i];
    }
    console.log(message);
}

// START SERVER

var PORT = env.NODE_PORT || 3000;
var IP = env.NODE_IP || 'localhost';

server.listen(PORT, IP, function () {
    cLog("Listening on " + IP + ":" + PORT)
});