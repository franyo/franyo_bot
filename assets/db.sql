DROP TABLE IF EXISTS bot_user;
DROP TABLE IF EXISTS received_message;
DROP TABLE IF EXISTS sent_message;

CREATE TABLE bot_user (
  fbuid        BIGINT NOT NULL,
  contact_time BIGINT NOT NULL,
  first_name   VARCHAR(1000),
  last_name    VARCHAR(1000),
  email        VARCHAR(1000),
  full_json    VARCHAR(1000),
  PRIMARY KEY (fbuid)
);

CREATE TABLE received_message (
  id            INT(255)      NOT NULL AUTO_INCREMENT,
  fbuid         BIGINT        NOT NULL,
  message_text  VARCHAR(1000) NOT NULL,
  full_content  VARCHAR(1000) NOT NULL,
  received_time BIGINT        NOT NULL,
  handled       BOOL          NOT NULL,
  PRIMARY KEY (id)
)
  AUTO_INCREMENT = 100;

CREATE TABLE sent_message (
  id           INT(255)      NOT NULL AUTO_INCREMENT,
  fbuid        BIGINT        NOT NULL,
  message_text VARCHAR(1000) NOT NULL,
  full_content VARCHAR(1000) NOT NULL,
  sent_time    BIGINT        NOT NULL,
  PRIMARY KEY (id)
)
  AUTO_INCREMENT = 100;