var mysql = require('mysql');
var connection;
var cLog;

var setup = function (env, logger) {
    connection = mysql.createConnection({
        host: env.OPENSHIFT_MYSQL_DB_HOST,
        port: env.OPENSHIFT_MYSQL_DB_PORT,
        user: 'adminAzY6gkI',
        password: '9Pu_ff-NYPSp',
        database: 'flxfbot'
    });
    cLog = logger;
};

var isFirstContact = function (fbuid, onFirstContact, onNotFirstContact) {
    var queryString = "SELECT * FROM bot_user WHERE bot_user.fbuid = ?";
    connection.query(queryString, [fbuid], function (err, rows) {
        if (!!err) {
            cLog("MySQL connection ERROR:" + err);
        } else if (rows.length == 0) {
            onFirstContact();
        } else {
            onNotFirstContact();
        }
    });
};

var saveBotUser = function (fbuid, contact_time, first_name, last_name, email, full_json) {
    var queryString = "INSERT INTO bot_user(fbuid, contact_time, first_name, last_name, email, full_json) VALUES(?,?,?,?,?,?)";
    connection.query(queryString, [fbuid, contact_time, first_name, last_name, email, full_json], function (err, result) {
        if (!!err) {
            cLog("MySQL connection ERROR:" + err);
        }
    });
};

var saveReceivedMessage = function (fbuid, message_text, full_content, received_time, handled, onSave) {
    var queryString = "INSERT INTO received_message(fbuid, message_text, full_content, received_time, handled) VALUES(?,?,?,?,?)";
    connection.query(queryString, [fbuid, message_text, full_content, received_time, handled], function (err, result) {
        if (!!err) {
            cLog("MySQL connection ERROR:" + err);
        } else {
            onSave();
        }
    });
};

var listBotUsers = function (onLoad) {
    var queryString = "SELECT * FROM bot_user";
    connection.query(queryString, [], function (err, rows) {
        if (!!err) {
            cLog("MySQL connection ERROR:" + err);
        } else {
            onLoad(rows);
        }
    });
};

module.exports.setup = setup;
module.exports.isFirstContact = isFirstContact;
module.exports.saveBotUser = saveBotUser;
module.exports.saveReceivedMessage = saveReceivedMessage;
module.exports.listBotUsers = listBotUsers;