var millisToDate = function (millis) {
    var date = new Date(millis);

    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    if (month < 10) {
        month = "0" + month;
    }
    var dayOfTheMonth = date.getDate();
    if (dayOfTheMonth < 10) {
        dayOfTheMonth = "0" + dayOfTheMonth;
    }

    var dateString = year + "." + month + "." + dayOfTheMonth;
    return dateString;
};

var millisToTime = function (millis) {
    var date = new Date(millis);

    var hour = date.getHours();
    if (hour < 10) {
        hour = "0" + hour;
    }
    var min = date.getMinutes();
    if (min < 10) {
        min = "0" + min;
    }
    var sec = date.getSeconds();
    if (sec < 10) {
        sec = "0" + sec;
    }

    var timeString = hour + ":" + min + ":" + sec;
    return timeString;
};

var millisToDateTime = function (millis) {
    var timeStampString = millisToDate(millis) + " " + millisToTime(millis);
    return timeStampString;
};

module.exports.millisToDate = millisToDate;
module.exports.millisToTime = millisToTime;
module.exports.millisToDateTime = millisToDateTime;